<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Author;
use App\Models\Book;
use App\Models\Publisher;
use App\Models\BookAuthor;
use App\Models\BookPublisher;

class BookSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        //Books fake data
        foreach(range(1,60) as $index){
            \DB::table('books')->insert([
                'title' => $faker->sentence,
                'description' => $faker->text($maxNbChars = 150)
            ]);
        }
        //Authors fake data
        foreach(range(1,45) as $index){
            \DB::table('authors')->insert([
                'name' => $faker->name
            ]);
        }
        //Publisher fake data
        foreach(range(1,30) as $index){
            \DB::table('publishers')->insert([
                'name' => $faker->company
            ]);
        }

        foreach(Book::all() as $book){
            \DB::table('book_author')->insert([
                'book_id' => $book['id'],
                'author_id' => Author::inRandomOrder()->first()->id
            ]);

            \DB::table('book_publisher')->insert([
                'book_id' => $book['id'],
                'publisher_id' => Publisher::inRandomOrder()->first()->id
            ]);
        }
    }
}
