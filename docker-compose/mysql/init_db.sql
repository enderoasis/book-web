-- MySQL dump 10.13  Distrib 5.7.25, for Win64 (x86_64)
--
-- Host: localhost    Database: library_db
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Dolly Watsica',NULL,NULL),(2,'Ms. Eloisa Hyatt Sr.',NULL,NULL),(3,'Marlin Bartell',NULL,NULL),(4,'Dr. Leslie Hickle II',NULL,NULL),(5,'Lucio Dare',NULL,NULL),(6,'Ada Hayes',NULL,NULL),(7,'Charley Towne',NULL,NULL),(8,'Rosa Batz',NULL,NULL),(9,'Brady Runolfsson',NULL,NULL),(10,'Mrs. Myrtis Rutherford',NULL,NULL),(11,'Josiane Kuphal Jr.',NULL,NULL),(12,'Mr. Nicholaus Ondricka III',NULL,NULL),(13,'Mr. Khalil Hermiston III',NULL,NULL),(14,'Miss Teresa Rath DDS',NULL,NULL),(15,'Ebony Breitenberg DVM',NULL,NULL),(16,'Heaven Stark III',NULL,NULL),(17,'Bertha Blick',NULL,NULL),(18,'Hilbert Smitham',NULL,NULL),(19,'Yadira Mann',NULL,NULL),(20,'Mrs. Geraldine Gutkowski',NULL,NULL),(21,'Nick Weissnat',NULL,NULL),(22,'Stacy Lynch',NULL,NULL),(23,'Prof. Giovanni Lakin',NULL,NULL),(24,'Prof. Hallie Stroman DDS',NULL,NULL),(25,'Valentina Morar I',NULL,NULL),(26,'Damon Schmitt',NULL,NULL),(27,'Damien Williamson',NULL,NULL),(28,'Jedidiah Beier',NULL,NULL),(29,'Antonetta Paucek',NULL,NULL),(30,'Prof. Liam Feest IV',NULL,NULL),(31,'Elfrieda Lesch',NULL,NULL),(32,'Scot Kunde',NULL,NULL),(33,'Ava Lockman DDS',NULL,NULL),(34,'Antonietta Roberts',NULL,NULL),(35,'Miss Kayli Wolf III',NULL,NULL),(36,'Shaylee Gutkowski III',NULL,NULL),(37,'Nikolas Fadel',NULL,NULL),(38,'Miss Brenda Schmitt DVM',NULL,NULL),(39,'Ford Prohaska PhD',NULL,NULL),(40,'Karina Marvin V',NULL,NULL),(41,'Elvie Stiedemann',NULL,NULL),(42,'Tessie Stamm',NULL,NULL),(43,'Mr. Tyrel Wisoky Jr.',NULL,NULL),(44,'Giovanni Kub',NULL,NULL),(45,'Miss Kaya Hermiston',NULL,NULL);
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_author`
--

DROP TABLE IF EXISTS `book_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_author` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) unsigned NOT NULL,
  `book_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_author_author_id_foreign` (`author_id`),
  KEY `book_author_book_id_foreign` (`book_id`),
  CONSTRAINT `book_author_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `book_author_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_author`
--

LOCK TABLES `book_author` WRITE;
/*!40000 ALTER TABLE `book_author` DISABLE KEYS */;
INSERT INTO `book_author` VALUES (1,33,1,NULL,NULL),(2,9,2,NULL,NULL),(3,19,3,NULL,NULL),(4,45,4,NULL,NULL),(5,6,5,NULL,NULL),(6,7,6,NULL,NULL),(7,18,7,NULL,NULL),(8,32,8,NULL,NULL),(9,3,9,NULL,NULL),(10,26,10,NULL,NULL),(11,8,11,NULL,NULL),(12,41,12,NULL,NULL),(13,18,13,NULL,NULL),(14,19,14,NULL,NULL),(15,19,15,NULL,NULL),(16,10,16,NULL,NULL),(17,19,17,NULL,NULL),(18,18,18,NULL,NULL),(19,19,19,NULL,NULL),(20,41,20,NULL,NULL),(21,30,21,NULL,NULL),(22,25,22,NULL,NULL),(23,30,23,NULL,NULL),(24,7,24,NULL,NULL),(25,41,25,NULL,NULL),(26,33,26,NULL,NULL),(27,12,27,NULL,NULL),(28,15,28,NULL,NULL),(29,24,29,NULL,NULL),(30,25,30,NULL,NULL),(31,13,31,NULL,NULL),(32,32,32,NULL,NULL),(33,27,33,NULL,NULL),(34,41,34,NULL,NULL),(35,6,35,NULL,NULL),(36,21,36,NULL,NULL),(37,9,37,NULL,NULL),(38,6,38,NULL,NULL),(39,25,39,NULL,NULL),(40,40,40,NULL,NULL),(41,18,41,NULL,NULL),(42,20,42,NULL,NULL),(43,19,43,NULL,NULL),(44,39,44,NULL,NULL),(45,11,45,NULL,NULL),(46,34,46,NULL,NULL),(47,43,47,NULL,NULL),(48,28,48,NULL,NULL),(49,18,49,NULL,NULL),(50,35,50,NULL,NULL),(51,23,51,NULL,NULL),(52,17,52,NULL,NULL),(53,36,53,NULL,NULL),(54,18,54,NULL,NULL),(55,30,55,NULL,NULL),(56,4,56,NULL,NULL),(57,42,57,NULL,NULL),(58,27,58,NULL,NULL),(59,18,59,NULL,NULL),(60,27,60,NULL,NULL);
/*!40000 ALTER TABLE `book_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_publisher`
--

DROP TABLE IF EXISTS `book_publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_publisher` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` bigint(20) unsigned NOT NULL,
  `publisher_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_publisher_book_id_foreign` (`book_id`),
  KEY `book_publisher_publisher_id_foreign` (`publisher_id`),
  CONSTRAINT `book_publisher_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE,
  CONSTRAINT `book_publisher_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `publishers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_publisher`
--

LOCK TABLES `book_publisher` WRITE;
/*!40000 ALTER TABLE `book_publisher` DISABLE KEYS */;
INSERT INTO `book_publisher` VALUES (1,1,10,NULL,NULL),(2,2,9,NULL,NULL),(3,3,10,NULL,NULL),(4,4,10,NULL,NULL),(5,5,30,NULL,NULL),(6,6,21,NULL,NULL),(7,7,16,NULL,NULL),(8,8,3,NULL,NULL),(9,9,29,NULL,NULL),(10,10,27,NULL,NULL),(11,11,25,NULL,NULL),(12,12,5,NULL,NULL),(13,13,30,NULL,NULL),(14,14,15,NULL,NULL),(15,15,25,NULL,NULL),(16,16,29,NULL,NULL),(17,17,11,NULL,NULL),(18,18,30,NULL,NULL),(19,19,28,NULL,NULL),(20,20,12,NULL,NULL),(21,21,20,NULL,NULL),(22,22,15,NULL,NULL),(23,23,27,NULL,NULL),(24,24,11,NULL,NULL),(25,25,24,NULL,NULL),(26,26,15,NULL,NULL),(27,27,29,NULL,NULL),(28,28,10,NULL,NULL),(29,29,6,NULL,NULL),(30,30,9,NULL,NULL),(31,31,2,NULL,NULL),(32,32,9,NULL,NULL),(33,33,7,NULL,NULL),(34,34,23,NULL,NULL),(35,35,18,NULL,NULL),(36,36,16,NULL,NULL),(37,37,18,NULL,NULL),(38,38,28,NULL,NULL),(39,39,27,NULL,NULL),(40,40,12,NULL,NULL),(41,41,18,NULL,NULL),(42,42,12,NULL,NULL),(43,43,4,NULL,NULL),(44,44,14,NULL,NULL),(45,45,15,NULL,NULL),(46,46,5,NULL,NULL),(47,47,6,NULL,NULL),(48,48,29,NULL,NULL),(49,49,26,NULL,NULL),(50,50,2,NULL,NULL),(51,51,29,NULL,NULL),(52,52,28,NULL,NULL),(53,53,9,NULL,NULL),(54,54,8,NULL,NULL),(55,55,10,NULL,NULL),(56,56,26,NULL,NULL),(57,57,20,NULL,NULL),(58,58,8,NULL,NULL),(59,59,2,NULL,NULL),(60,60,21,NULL,NULL);
/*!40000 ALTER TABLE `book_publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'Incidunt ipsa nisi magnam.','Rerum et officiis accusamus qui quisquam. Aut aut odio ratione id voluptatum quidem.',NULL,NULL),(2,'Laborum animi porro suscipit.','Ratione quae reprehenderit consequuntur porro aut eaque. Non et quia quam alias voluptas deleniti. Amet nisi eum error voluptatum.',NULL,NULL),(3,'Dicta repellat assumenda nisi omnis quasi saepe voluptatem.','Ut quis ut odit eligendi quia vero. Ab magnam nostrum magnam. Corrupti harum quo doloremque animi ea assumenda. Atque quasi ut eum.',NULL,NULL),(4,'Maiores quas doloribus reiciendis modi.','Autem esse sit fugiat dignissimos sint cumque eaque voluptatem. Qui facere quia autem est aliquam deleniti quam.',NULL,NULL),(5,'Quae ad eum iste architecto enim assumenda sapiente.','Nam vel qui soluta dolore libero consequatur. Unde impedit error a fugiat. Delectus nulla culpa omnis.',NULL,NULL),(6,'Neque sed commodi ut laudantium dolorem.','Incidunt et asperiores eveniet voluptas. Quod placeat ullam iure. Minus non a aut consequatur quisquam aut.',NULL,NULL),(7,'Architecto aliquam similique eaque at minus ut ab quisquam.','Sint sit ut eaque amet. Modi reiciendis quis expedita eius accusamus. Quis atque id ut non quisquam.',NULL,NULL),(8,'Corrupti sint voluptas blanditiis porro reiciendis placeat.','Est consectetur deleniti quis sunt ex magni. Quis accusantium et ut tempore quis. Quod omnis eum perferendis vel quod vel ipsa.',NULL,NULL),(9,'Exercitationem quis aut illum dolore cupiditate illum.','Nihil vitae deserunt autem dolor. Adipisci ipsum quaerat aperiam eligendi.',NULL,NULL),(10,'Unde cum illum accusamus voluptatum.','Perspiciatis fugit eum distinctio nulla quia. Dolorem nostrum consequuntur in expedita aut. Qui tenetur nihil ut.',NULL,NULL),(11,'Provident dolorem laborum saepe aperiam sed molestiae.','Molestiae sequi qui sed dolor velit eius. Doloribus dolorum nostrum expedita consequatur veritatis hic.',NULL,NULL),(12,'Maxime quia doloribus harum et.','Dolorem est qui qui molestiae inventore. Consectetur exercitationem itaque iure quidem. Dolorem sint assumenda aut itaque ut.',NULL,NULL),(13,'Esse vero sit est quo numquam aut.','Id dolorum eum aperiam magni accusantium nostrum dolore. Sapiente at distinctio aut placeat eveniet. Qui earum et ut vel ut.',NULL,NULL),(14,'Cumque impedit sed totam nulla consequuntur eos.','Pariatur modi non hic eum aut qui distinctio veniam. Nulla earum sed eos eos quis.',NULL,NULL),(15,'Et et quasi placeat et.','Est veritatis eius quidem qui. Distinctio temporibus aliquid quaerat repellat sed beatae. Unde id cupiditate corporis voluptate sapiente iste.',NULL,NULL),(16,'Voluptatem tempore sit eos laudantium.','Omnis quia nobis saepe ut quae velit. Totam omnis molestiae ut illum repudiandae natus veniam. Tenetur hic doloremque et iusto qui nostrum dolor.',NULL,NULL),(17,'Repellendus et nihil repellat quae ut in quis.','Ipsam quis vel amet doloribus maiores libero rerum. Sit quae voluptatem odit non. Porro exercitationem quis sed qui.',NULL,NULL),(18,'Aut aut quidem facilis maiores repudiandae sit voluptatibus esse.','Magni ex officia distinctio aperiam rerum et. Distinctio dolorem ut tempora numquam.',NULL,NULL),(19,'Quos numquam voluptates eum aut atque.','Error error voluptas natus eveniet molestiae. Qui quia illo sed sunt. Ut nam et et excepturi animi vel labore. Quibusdam veniam eum et sint.',NULL,NULL),(20,'Nisi dolorem et libero deserunt numquam voluptas.','Totam eos officia eos molestias et. Qui reprehenderit minima voluptatem. Est laudantium nihil quos consequuntur deserunt soluta et.',NULL,NULL),(21,'Impedit quo voluptates est blanditiis iste accusantium.','Eligendi amet quod et rerum. Quia et quasi eos. Facilis et hic labore. Enim sapiente omnis laudantium iste.',NULL,NULL),(22,'Veniam consequatur ullam quidem provident nostrum repudiandae assumenda.','Est aut distinctio non quasi ut. Omnis distinctio ut et minima dolore vel. Voluptas quidem dolorem tenetur accusantium nostrum quam.',NULL,NULL),(23,'Voluptatem eius maiores dolores.','Harum fugiat provident perferendis dolor esse qui ea autem. Reprehenderit velit vitae corporis vitae quia et odio rem. Est eos eum inventore quis.',NULL,NULL),(24,'Tempore aut temporibus qui est et ea.','Et modi itaque assumenda autem molestiae temporibus. Qui voluptate aliquid debitis aut suscipit eius. Modi neque consequatur voluptas dolor quos aut.',NULL,NULL),(25,'Dicta quasi nostrum sapiente voluptas qui perferendis est.','Nulla quo eveniet suscipit. Ad iure autem sunt eos dicta quia sit ea. Et qui rem rerum. Et dolorem corrupti labore nihil.',NULL,NULL),(26,'Earum labore quia quia molestias ipsa eum voluptatem.','At sint vel quod optio. Vero corrupti sunt cumque eos esse assumenda non. Voluptatem est voluptatem a sed voluptate impedit animi.',NULL,NULL),(27,'Eos sed et sed fugit.','Quibusdam sit quae perspiciatis consectetur ea accusantium eveniet. Dolor quod in est qui enim. Sequi accusantium nostrum dolor necessitatibus.',NULL,NULL),(28,'Pariatur pariatur quod qui in eligendi dicta quaerat.','Mollitia omnis qui in minus doloribus. Molestiae ab iste vitae ducimus et quibusdam.',NULL,NULL),(29,'Est sequi et eum earum ipsa dolor velit.','Ut est amet sit neque repellat. Aut sint unde dolorem necessitatibus optio et. Amet fugiat ut laborum ut. Sit est tempore labore dolor.',NULL,NULL),(30,'At odit minima qui nulla vitae aut alias.','Dolores unde et eos fugit error id. Autem modi sit qui dolores. Rerum suscipit ut laborum quidem dolorem eos labore repellendus.',NULL,NULL),(31,'Molestiae impedit nobis aut maiores et odio incidunt.','Et rerum vel quia corporis libero. Aut molestias modi aut reprehenderit delectus commodi hic. Impedit quidem minima porro aliquam.',NULL,NULL),(32,'Enim dignissimos est et suscipit.','Repellat odit aut dolor voluptas omnis illum excepturi distinctio. Aut non dolore ullam. Accusantium cupiditate omnis et ut quia ab est iusto.',NULL,NULL),(33,'Perspiciatis officia architecto corporis maxime dolore fuga fugit consequatur.','Necessitatibus enim repellendus autem dolorem illo est tempore. Et quia aliquid numquam.',NULL,NULL),(34,'Consequatur rerum dolor rem tempora.','Sit quidem architecto quam commodi ipsa unde. Officiis a repellat est quidem est et deleniti. Illo deserunt dolorum aspernatur occaecati non.',NULL,NULL),(35,'Aliquam suscipit quo et ratione et.','Eius distinctio cum odio minima. Facere rerum asperiores libero velit deserunt maxime quo. Nihil dignissimos illum quasi quisquam.',NULL,NULL),(36,'Sit sed placeat rem rerum vel illo.','Eum explicabo expedita harum. Voluptatem sit rerum soluta velit.',NULL,NULL),(37,'Facere eos id nam delectus quibusdam nisi veniam.','Sed et et sit voluptate aliquid voluptatem et. Hic aut ratione dolore illo. Voluptate aperiam voluptas et repellat dolores voluptatibus.',NULL,NULL),(38,'Dolor velit ab nostrum corrupti autem dolorum aut.','Nulla rerum perferendis neque. Ad blanditiis non repellendus voluptatem in et.',NULL,NULL),(39,'Totam vero ut blanditiis eaque.','Vel asperiores dolor ea nobis. Dolores et id officiis. Est aliquid quo id dignissimos. Voluptas illo consequatur iusto exercitationem occaecati.',NULL,NULL),(40,'Tenetur doloribus ut qui fugit enim.','In et dolor consequatur eveniet cupiditate. Similique quod saepe quis. Ut voluptatem cum est ea. Aliquid numquam deserunt ut unde.',NULL,NULL),(41,'Reprehenderit et placeat minima accusamus illo quia similique veniam.','Quas dicta in alias eius alias quia qui. Sint animi dolorum quia incidunt. Occaecati aut ut ut quaerat rerum accusantium.',NULL,NULL),(42,'Ipsa velit quidem id molestiae fuga nam non.','Explicabo voluptas quia voluptas harum soluta tenetur qui ea. Dolor quis et eum mollitia. Vel est amet et ipsum molestiae. Aut et similique ut quia.',NULL,NULL),(43,'Magni quibusdam sed minus maxime voluptatibus dignissimos.','Tempore nihil repudiandae veritatis quasi. Eos consectetur ab aut natus est sit. Aut unde sit et.',NULL,NULL),(44,'Aliquid laboriosam aut laboriosam quibusdam.','Laudantium debitis iusto atque rem. Assumenda in necessitatibus quidem et sed. Ipsam et molestiae et consectetur fugiat et rerum.',NULL,NULL),(45,'Rerum earum qui fugiat.','Quam consequatur reprehenderit ut consequatur fugiat. Ipsam sit dolores at dolore est et. Minima non autem et omnis.',NULL,NULL),(46,'Magnam et aliquam ad nihil eius tenetur similique.','Sed ad vel quod porro. Inventore impedit voluptatem distinctio laboriosam ut. Facilis deleniti quos voluptate reprehenderit.',NULL,NULL),(47,'Qui quidem quod veniam eos.','Non minima ea quisquam. Doloremque corporis ea possimus odit. Enim nihil quaerat quia ratione non rem suscipit. Et dolores harum in quam quia quo.',NULL,NULL),(48,'Tenetur et minus porro eligendi.','Voluptas voluptas nihil et alias ut explicabo. Illum omnis veritatis dolor nihil laborum ducimus alias. Placeat commodi aut ipsam vel.',NULL,NULL),(49,'Numquam est omnis id sit et earum nobis provident.','Ullam aut qui a sapiente eos quia animi. Perspiciatis cupiditate et veniam et in vel. Ut consequuntur dolor eveniet excepturi officia quia.',NULL,NULL),(50,'Iure qui nam ex minus.','A sequi ad quis tenetur ipsum numquam in. Voluptatem rerum tenetur et rerum eum. Deleniti sint molestias sit. Blanditiis eos mollitia laboriosam.',NULL,NULL),(51,'Aut reprehenderit et aut at corrupti explicabo et vitae.','Rerum qui nihil harum. Commodi repudiandae amet consequatur quis non enim.',NULL,NULL),(52,'Iure ratione ut est quia ut.','Aut reprehenderit accusantium sunt sequi est. Dolores ducimus corporis necessitatibus et repellat unde. Doloremque et deserunt tenetur.',NULL,NULL),(53,'Ut sit maiores cumque voluptates fuga.','Autem rerum optio voluptates autem aut accusantium mollitia. Et eum dolor voluptas sequi rerum voluptatem fuga. Ratione numquam vel sit odio nihil.',NULL,NULL),(54,'Et debitis minus repellat itaque culpa ex.','Odit occaecati eum laborum magnam neque sit voluptas. Qui dolores cumque quis nobis.',NULL,NULL),(55,'Sapiente minus numquam et facere sit dolorem.','Autem nulla sit a animi et nostrum asperiores. Numquam rerum aut voluptas ipsum. Similique ab est aut sed. Commodi fuga esse voluptatem dolor.',NULL,NULL),(56,'Et enim enim est cupiditate et accusantium occaecati.','Esse ut aut quo optio eaque aut quas. Et autem necessitatibus qui non odit dolorum. Reiciendis aliquam eaque fuga et ullam accusamus quidem.',NULL,NULL),(57,'Iste aut corporis nam rerum quos reprehenderit quo.','Earum nihil et officiis atque voluptatibus quae. Et laudantium blanditiis quo pariatur. In harum vitae aut sint.',NULL,NULL),(58,'Dolorem animi id enim sequi tempore dolores.','Expedita modi nemo quas vel. Quam itaque accusantium impedit ea omnis. Ex rem sapiente et perferendis error. Quia provident facilis culpa nemo.',NULL,NULL),(59,'Quis qui ad inventore sunt quaerat consequatur nihil.','Ut mollitia odit deserunt et non et. Eos sit sunt qui. Exercitationem nihil optio animi nostrum quo ipsam iste.',NULL,NULL),(60,'Commodi occaecati quo reiciendis dolorem commodi.','Laudantium qui adipisci quasi maiores. Perferendis nam impedit sit et. Dignissimos minus aut nobis minima laboriosam dolores.',NULL,NULL);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2022_03_10_122457_create_books_table',1),(5,'2022_03_10_122515_create_authors_table',1),(6,'2022_03_10_122530_create_publishers_table',1),(7,'2022_03_10_122834_create_book_author_table',1),(8,'2022_03_10_122847_create_book_publisher_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publishers`
--

DROP TABLE IF EXISTS `publishers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publishers`
--

LOCK TABLES `publishers` WRITE;
/*!40000 ALTER TABLE `publishers` DISABLE KEYS */;
INSERT INTO `publishers` VALUES (1,'Kunze, Krajcik and Hamill',NULL,NULL),(2,'Price, Rogahn and Towne',NULL,NULL),(3,'Kuhn, Steuber and Gislason',NULL,NULL),(4,'Ebert-Luettgen',NULL,NULL),(5,'Friesen Group',NULL,NULL),(6,'Ankunding LLC',NULL,NULL),(7,'Gorczany, Bosco and Schaefer',NULL,NULL),(8,'Barton, Ullrich and Reichert',NULL,NULL),(9,'Gibson, Anderson and Smitham',NULL,NULL),(10,'Ullrich Inc',NULL,NULL),(11,'Zboncak, Homenick and Mante',NULL,NULL),(12,'Kub-Tillman',NULL,NULL),(13,'Okuneva Group',NULL,NULL),(14,'Stanton LLC',NULL,NULL),(15,'Hyatt, McGlynn and Powlowski',NULL,NULL),(16,'Bartoletti and Sons',NULL,NULL),(17,'Rosenbaum Inc',NULL,NULL),(18,'Schamberger-Christiansen',NULL,NULL),(19,'Lindgren, Bartell and Moore',NULL,NULL),(20,'Moen-Abbott',NULL,NULL),(21,'Cummerata, Jacobi and Rosenbaum',NULL,NULL),(22,'Lowe-Kilback',NULL,NULL),(23,'Kerluke PLC',NULL,NULL),(24,'Paucek, Swaniawski and Jones',NULL,NULL),(25,'O\'Kon-Spencer',NULL,NULL),(26,'Moen, Smitham and Nienow',NULL,NULL),(27,'McDermott-Berge',NULL,NULL),(28,'Sanford, D\'Amore and Marks',NULL,NULL),(29,'Veum-Bailey',NULL,NULL),(30,'Beahan-Yundt',NULL,NULL);
/*!40000 ALTER TABLE `publishers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Test User','test@gmail.com',NULL,'$2y$10$ESVkonGtRAsHU7q/9/LuzO1GXm8f5ayAa7AjL1mXkKPJNxOnEfE0G',NULL,'2022-03-10 09:19:22','2022-03-10 09:19:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-07 18:55:23
