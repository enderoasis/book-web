# Title

Book Store Interview Tech

## Description

Есть несколько издательских сайтов по продаже книг, которые хотели бы видеть свои книги в Библиотеке. При этом разные издательства могут печатать одинаковые книги. У книг может быть несколько авторов, а у авторов несколько книг.<br>

Что нужно сделать обязательно?<br>
Написать публичное RESTful API (далее API) для издательских сайтов, которое позволит добавлять, изменять и удалять книги из списка Библиотеки.<br>

Что можно сделать дополнительно?<br>
1. Закрыть API авторизацией.<br>
2. Создать интерактивную документацию API.<br>
3. Вывести на главную страницу Библиотеки список книг. Каждая строка списка должна содержать название книги, имена авторов и название издательства.<br>


### Tech requirments

* Laravel 7+
* Bootstrap 4+


### Auto configuration

* Docker 
```
1) docker-compose build app
2) docker-compose up -d
```

### Manual configuration

* Configure your database: <br>
Recommended: MySQL 5.7<br>
Create database for this project and paste credentials to .env file

* Run following commands:
```
composer install
php artisan key:generate
php artisan jwt:secret
php artisan migrate:fresh --seed

Type this command in console and keep it running:
php artisan serve

Then you can visit these links:

You can test REST API via this: http://localhost:8000/api/documentation
You can view list of book with its authors,publishers via this: http://localhost:8000/
```

### Test User credentials

Login: test@gmail.com
Password: 123123

## Author

Контактное лицо

Didar Temirkhanov
Telegram. [@temirkhanov.d](https://t.me/temirkhanov.d) <br>
LinkedIn. [@temirkhanov.d](https://www.linkedin.com/in/didar-temirkhanov/)
