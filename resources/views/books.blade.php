<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="page-wrapper">
    <div class="page-container">
        <div class="main-content">
            <div class="section__content section__content--p30">
                    <div class="container-fluid">
                         <table class="table table-bordered table-hover">
                            <thead class="thead-dark">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Book</th>
                                <th scope="col">Authors</th>
                                <th scope="col">Publishers</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($books as $book)
                                <tr>
                                <th scope="row">{{$book['id']}}</th>
                                <td>{{$book['title']}}</td>
                                @foreach($book['authors'] as $book_author)
                                <td>{{$book_author['name']}}</td>
                                @endforeach
                                
                                @foreach($book['authors'] as $book_publisher)
                                <td>{{$book_publisher['name']}}</td>
                                @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
         </div>
    </div>
</div>
</body>
</html>