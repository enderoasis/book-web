<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title','description'];
    
    protected $hidden = ['created_at','updated_at','pivot'];

    public function publishers()
    {
        return $this->belongsToMany(Publisher::class, 'book_publisher');
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class, 'book_author');
    }
}
