<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookPublisher extends Model
{
    use HasFactory;

    protected $table = 'book_publisher';
}
