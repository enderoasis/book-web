<?php

namespace App\Services;

use App\Models\Book;
use App\Models\Publisher;
use App\Models\Author;
use App\Http\Requests\BookCreateRequest;
use App\Resources\BookResource;
use Illuminate\Http\Request;

class BookService 
{
    public static function all()
    {
        $books = Book::with(['authors', 'publishers'])->get();

        return $books;

    }

    public function create(BookCreateRequest $request)
    {
        if($request->validator && $request->validator->fails())
        {
            return $request->validator->messages();
        }

        $book = Book::create($request->all());

        return response()->json($book, 201); 

    }

    public function update(Request $request, $id)
    {
       $book = Book::findorfail($id);

       $book->update($request->all());

       return response()->json($book, 201); 
    }

    public function one($id)
    {
        $book = Book::with('authors', 'publishers')->find($id);
        
        return response()->json($book, 200); 

    }

	public function delete($id)
    {
        $book = Book::findorfail($id);
        $book->delete();

        return response()->json('Book has been deleted', 200); 
    }
}