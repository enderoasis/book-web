<?php

namespace App\Services;

use App\Models\User;
use JWTAuth;
use App\Http\Requests\AuthRequest;

class AuthorizationService 
{	
    public function authorization(AuthRequest $request)
    {
        if($request->validator && $request->validator->fails())
        {
            return $request->validator->messages();
        }

        $token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password]);

        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'user' => auth()->user(),
        ],200);
    }
}