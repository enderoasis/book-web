<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookCreateRequest;
use App\Services\BookService;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
    * @OA\Get(
    *      path="/api/v1/books",
    *      operationId="v1/books",
    *      tags={"Books"},
    *      summary="Get Book List",
    *      security={
    *          {"bearerAuth": {}},
    *      },
    *      @OA\Response(
    *         response="200",
    *         description="Everything is fine",
    *         content={
    *             @OA\MediaType(
    *                 mediaType="application/json",
    *                 @OA\Schema(
    *                     @OA\Property(
    *                         property="success",
    *                         type="boolean",
    *                         description="The response code"
    *                     ),
    *                 )
    *              )
    *           }
    *        ),
    *     )
    */
    public function index()
    {
        return (new BookService)->all();    
    }

    /**
    * @OA\Post(
    *     path="/api/v1/books",
    *     operationId="books/book-create",
    *      tags={"Books"},
    *     summary="Book add",
    *     security={
    *         {"bearerAuth": {}},
    *     },
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\MediaType(
    *             mediaType="application/json",
    *             @OA\Schema(
    *                 example={
    *                         "title": "Peace and War",
    *                         "description": "It is about Napoleonic wars,kinda long to describe, better to read",
    *                 }
    *             )
    *         )
    *     ),
    *      @OA\Response(
    *         response="201",
    *         description="Everything is fine",
    *         content={
    *             @OA\MediaType(
    *                 mediaType="application/json",
    *                 @OA\Schema(
    *                     @OA\Property(
    *                         property="success",
    *                         type="boolean",
    *                         description="The response code"
    *                     ),
    *                 )
    *              )
    *           }
    *        ),
    *     )
    * )
    */
    public function store(BookCreateRequest $request)
    {
        return (new BookService)
            ->create($request);
    }

    /**
    * @OA\Patch(
    *     path="/api/v1/books/{id}",
    *     operationId="books/book-edit",
    *      tags={"Books"},
    *     summary="Book edit",
    * @OA\Parameter(
    *          name="id",
    *          description="book id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *     security={
    *         {"bearerAuth": {}},
    *     },
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\MediaType(
    *             mediaType="application/json",
    *             @OA\Schema(
    *                 example={
    *                         "title": "Jungle Book",
    *                         "description": "It is about little boy who was raised by wolves and had as friends bear and puma",
    *                 }
    *             )
    *         )
    *     ),
    *      @OA\Response(
    *         response="200",
    *         description="Everything is fine",
    *         content={
    *             @OA\MediaType(
    *                 mediaType="application/json",
    *                 @OA\Schema(
    *                     @OA\Property(
    *                         property="success",
    *                         type="boolean",
    *                         description="The response code"
    *                     ),
    *                 )
    *              )
    *           }
    *        ),
    *     )
    * )
    */
    public function update(Request $request, $id)
    {
        return (new BookService)
            ->update($request, $id);
    }

    /**
    * @OA\Get(
    *      path="/api/v1/books/{id}",
    *      operationId="books/bookById",
    *      tags={"Books"},
    *      summary="Get Book one (show)",
    *      @OA\Parameter(
    *          name="id",
    *          description="book id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      security={
    *          {"bearerAuth": {}},
    *      },
    *      @OA\Response(
    *         response="200",
    *         description="Everything is fine",
    *         content={
    *             @OA\MediaType(
    *                 mediaType="application/json",
    *                 @OA\Schema(
    *                     @OA\Property(
    *                         property="success",
    *                         type="boolean",
    *                         description="The response code"
    *                     ),
    *                 )
    *              )
    *           }
    *        ),
    *     )
    */
    public function show($id)
    {
        return (new BookService)
            ->one($id);
    }

    /**
    * @OA\Delete(
    *      path="/api/v1/books/{id}",
    *      operationId="books/book-delete",
    *      tags={"Books"},
    *      summary="Delete a book",
    *      @OA\Parameter(
    *          name="id",
    *          description="book id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *      ),
    *      security={
    *          {"bearerAuth": {}},
    *      },
    *      @OA\Response(
    *         response="200",
    *         description="Everything is fine",
    *         content={
    *             @OA\MediaType(
    *                 mediaType="application/json",
    *                 @OA\Schema(
    *                     @OA\Property(
    *                         property="success",
    *                         type="boolean",
    *                         description="The response code"
    *                     ),
    *                 )
    *              )
    *           }
    *        ),
    *     )
    */

    public function destroy($id)
    {
        return (new BookService)
            ->delete($id);
    }
}
