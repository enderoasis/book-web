<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Services\AuthorizationService;

class AuthController extends Controller
{
     /**
    * @OA\Post(
    * path="/api/v1/login",
    * summary="Login",
    * description="Login by email, password",
    * operationId="authLogin",
    * tags={"Authorization"},
    * @OA\RequestBody(
    *    required=true,
    *    description="Pass user credentials",
    *    @OA\JsonContent(
    *       required={"email","password"},
    *       @OA\Property(property="email", type="string", format="email", example="test@gmail.com"),
    *       @OA\Property(property="password", type="string", format="password", example="123123"),
    *    ),
    * ),
    * @OA\Response(
    *    response=422,
    *    description="Wrong credentials response",
    *    @OA\JsonContent(
    *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    *        )
    *     )
    * )
    */
    public function authorization(AuthRequest $request)
    {
        return (new AuthorizationService())
            ->authorization($request);
    }
}
